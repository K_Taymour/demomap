import React from 'react';
import { connect } from 'react-redux';
import { loadMap } from '../../utils/GISHelper';
import FeatureLayerHandler from '../../shared/FeatureLayerHandler';

class EsriMap extends React.PureComponent {

  constructor(props) {
    super(props);
    this.mapNode = React.createRef();
  }

  componentDidMount() {
    const options = {
      basemap: 'streets',
      center: [-118, 34],
      zoom: 8
    };
    loadMap(this.mapNode.current, options).then(view => {
      this._view = view;
      this._featureLayerHandler = new FeatureLayerHandler(this._view, "calls");
    });
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.calls !== nextProps.calls && this._view) {
      this._featureLayerHandler.load(nextProps.calls);
    }
    return false;
  }

  // destroy the map before this component is removed from the DOM
  componentWillUnmount() {
    if (this._view) {
      this._view.container = null;
      delete this._view;
    }
  }
  render() {
    return <div style={{ height: '100%', width: '100%' }} ref={this.mapNode} id="test" />;
  }
}

function mapStatetoProps(state) {
  return {
    calls: state.calls.calls
  }
}

export default connect(mapStatetoProps)(EsriMap);
