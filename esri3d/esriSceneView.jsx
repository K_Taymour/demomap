// import React, { Component } from 'react';
// import { SceneView, Scene, Layer, UI, Ground, Graphic, Legend, SliceTool, DistanceMeasurementTool, DrawingTool } from 'react-sceneview';

// export default class EsriSceneView extends Component {
//     render() {
//         const initialViewProperties = {
//             viewpoint: {
//                 camera: {
//                     position: {
//                         latitude: 51.48391,
//                         longitude: -0.184,
//                         z: 10491.931965556927,
//                     },
//                     tilt: 0,
//                     heading: 355,
//                 },
//             },
//         };

//         let polygon = {
//             type: "polygon", // autocasts as new Polygon()
//             rings: [
//                 [-0.184, 51.48391, 400],
//                 [-0.184, 51.49091, 500],
//                 [-0.172, 51.49091, 500],
//                 [-0.172, 51.48391, 400],
//                 [-0.184, 51.48391, 400]
//             ]
//         };

//         let fillSymbol = {
//             type: "simple-fill", // autocasts as new SimpleFillSymbol()
//             color: [227, 139, 79, 0.8],
//             outline: {
//                 // autocasts as new SimpleLineSymbol()
//                 color: [255, 255, 255],
//                 width: 1
//             }
//         };

//         let attr = [{ id: 1 }, { name: 'a' }];

//         let fields = ['id', 'name']

//         return (
//             <SceneView
//                 id="sceneview"
//             // onLoad={e => console.log(e)}
//             >
//                 <UI.Zoom />
//                 <UI.Compass />
//                 <UI.NavigationToggle />
//                 <Scene
//                     basemap="dark-gray-vector"
//                     initialViewProperties={initialViewProperties}
//                     ground="world-elevation"
//                 // onLoad={e => console.log(e)}
//                 >

//                     {/* <Ground
//                         opacity={0.5}
//                         navigationConstraint={{ type: 'none' }}
//                     /> */}
//                     {/* <Layer
//                         id="buildings"
//                         layerType="scene"
//                         selectable
//                         url="https://tiles.arcgis.com/tiles/P3ePLMYs2RVChkJx/arcgis/rest/services/Building_Montreal/SceneServer"
//                     // onLoad={e => console.log(e)}
//                     /> */}

//                     <Layer
//                         id="polygon"
//                         layerType="feature"
//                         geometryType="polygon"
//                         objectIdField="OID"
//                     >
//                         <Graphic
//                             geometry={{polygon}}
//                             symbol={{fillSymbol}}
//                             attributes={null}
//                         />
//                     </Layer>
//                     <Legend />


//                 </Scene>
//             </SceneView>
//         )
//     }
// };



import { loadCss } from 'esri-loader';
import React, { Component } from 'react';
import { Map, Scene } from '@esri/react-arcgis';
import MyEsriGraphics from './MyEsriGraphics'

export default class EsriSceneView extends Component {
    render() {
        // by default loadCss() loads styles for the latest 4.x version
        loadCss();

        return (
            <Scene
                mapProperties={{ basemap: 'streets' }}
                viewProperties={{
                    center: [-0.184, 51.48],
                    zoom: 6
                }}
            >
                <MyEsriGraphics />
            </Scene>
        );
    }
}