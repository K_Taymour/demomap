import React from "react";

const InfoBoxTemplate = ({ el, centerAndZoom, closeInfoBox }) => {
  return (
    <div className="esri-popup__main-container esri-widget esri-popup--is-collapsible">
      <button onClick={closeInfoBox}>Close</button>
      <table
        className="esri-widget__table"
        summary="List of attributes and values"
      >
        <tbody>
          <tr>
            <th className="esri-feature-fields__field-header">Address</th>
            <td className="esri-feature-fields__field-data">{el.address}</td>
          </tr>
          <tr>
            <th className="esri-feature-fields__field-header">X Cord</th>
            <td className="esri-feature-fields__field-data">{el.xCord}</td>
          </tr>
          <tr>
            <th className="esri-feature-fields__field-header">Y Cord</th>
            <td className="esri-feature-fields__field-data">{el.yCord}</td>
          </tr>
        </tbody>
      </table>
      <button
        onClick={() => {
          centerAndZoom({
            latitude: el.yCord,
            longitude: el.xCord,
          });
        }}
      >
        Zoom
      </button>
    </div>
  );
};

export default InfoBoxTemplate;
