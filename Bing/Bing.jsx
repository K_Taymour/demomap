import React, { useState } from "react";
import ReactBingMap, {
    Pushpin,
    Polyline,
    Layer,
    ClusterLayer,
    Infobox,
    // Polygon,
} from "@3acaga/react-bing-maps";
// import { useSelector } from "react-redux";
import InfoBoxTemplate from "./InfoBoxTemplate";
import icon from "./IncIcon.png";
import './Bing.css';

const key = "Aqv4vDnnMoAMtqI_0uMOUA_QOFsY4KMtyXXbw4yHPUG0WSjDKLweMxktRqm3mUGA";

const start = {
    latitude: 35,
    longitude: -120,
};

const end = {
    latitude: 33,
    longitude: -125.5,
};

const polygon = [
    { longitude: -118.41491542974485, latitude: 32.796222697906565 },
    { longitude: -118.41574817473702, latitude: 32.796065819445218 },
    { longitude: -118.41654141950491, latitude: 32.796065270943629 },
    { longitude: -118.41662684083538, latitude: 32.795557149977014 },
    { longitude: -118.41639516666771, latitude: 32.795205566730765 },
    { longitude: -118.4153309050153, latitude: 32.79520629163089 },
    { longitude: -118.41482051666702, latitude: 32.795249744456967 },
    { longitude: -118.41468349477175, latitude: 32.795636623484093 },
    { longitude: -118.41491542974485, latitude: 32.796222697906565 },
];

const borderedPushPin = `<svg xmlns="http://www.w3.org/2000/svg" width="60" height="60">
<circle cx="30" cy="30" r="25" stroke="{color}" stroke-width="2" fill="none" />
<circle cx="30" cy="30" r="20" stroke="{color}" stroke-width="1" fill="none" />
<image href="${icon}" x="10" y="10" height="40" width="40"/>
</svg>`;

const buffer = `<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100">
<circle cx="50" cy="50" r="50" stroke="{color}" stroke-width="2" fill="{fill}" />
</svg>`;

function clusteredPinCallback(cluster) {
    //Define variables for minimum cluster radius, and how wide the outline area of the circle should be.
    var minRadius = 12;
    //Get the number of pushpins in the cluster
    var clusterSize = cluster.containedPushpins.length;
    //Calculate the radius of the cluster based on the number of pushpins in the cluster, using a logarithmic scale.
    var radius = (Math.log(clusterSize) / Math.log(10)) * 5 + minRadius;
    console.log('radius', radius)

    //Customize the clustered pushpin using the generated SVG and anchor on its center.
    cluster.setOptions({
        icon: icon,
        anchor: { x: radius, y: radius },
        textOffset: { x: 0, y: radius + 8 },
        // icon: svg.join(""),
        // anchor: new Microsoft.Maps.Point(radius, radius),
        // textOffset: new Microsoft.Maps.Point(0, radius - 8), //Subtract 8 to compensate for height of text.
    });
}

const MapBing3 = () => {
    // const data = useSelector((state) => state.data);
    const data = [
        {
            id: 1,
            address: "2695 APT5 Rolling Hills DR Saint Augustine FL ",
            yCord: 34.093382,
            xCord: -118.373544,
        },
        {
            id: 2,
            address: "Incident in Saint Augustine FL ",
            yCord: 45.566,
            xCord: -122.773,
        },
        {
            id: 3,
            address: "",
            yCord: 33.052203,
            xCord: -120.23822,
        },
    ];
    const [zoom, setZoom] = useState(8);
    const [center, setCenter] = useState({ latitude: 34, longitude: -118 });
    const [opened, setOpened] = useState(null);
    const [showPolygon, setShowPolygon] = useState(false);

    const centerAndZoomHandler = (center) => {
        setCenter(center);
        setZoom((prevZoom) => prevZoom + 2);
    };

    const closeInfoBox = () => {
        setOpened(null);
    };
    const togglePolygon = () => {
        if (!showPolygon) {
            setCenter({
                longitude: -118.41639516666771,
                latitude: 32.795205566730765,
            });
            setZoom(18);
        } else {
            setCenter({ latitude: 34, longitude: -118 });
            setZoom(8);
        }

        setShowPolygon(!showPolygon);
    };
    return (
        <div className="react-bingmaps">
            <button
                style={{ position: "absolute", zIndex: 1 }}
                onClick={togglePolygon}
            >
                show/hide
      </button>
            <ReactBingMap apiKey={key} center={center} zoom={zoom}>
                {!showPolygon && (
                    <>
                        {/* <ClusterLayer zIndex={1}>
              <Pushpin
                location={{ latitude: 34.093382, longitude: -118.373544 }}
                icon={buffer
                  .replace(/{color}/g, "red")
                  .replace(/{fill}/g, "rgba(255, 255, 255, 0.8)")}
                anchor={{ x: 50, y: 50 }}
              />
            </ClusterLayer> */}
                        <ClusterLayer clusteredPinCallback={clusteredPinCallback}>
                            {data.map((el) => (
                                <React.Fragment key={el.id}>
                                    <Pushpin
                                        location={{ latitude: el.yCord, longitude: el.xCord }}
                                        icon={
                                            el.id !== 2
                                                ? // ? "assets/IncIcon.png"
                                                borderedPushPin.replace(/{color}/g, "transparent")
                                                : borderedPushPin.replace(/{color}/g, "red")
                                        }
                                        anchor={{ x: 30, y: 30 }}
                                        typeName={"bla"}
                                        onClick={() => setOpened(el.id)}
                                        title={el.address}
                                    />
                                    {opened === el.id && (
                                        <Infobox
                                            location={{ latitude: el.yCord, longitude: el.xCord }}
                                        >
                                            <InfoBoxTemplate
                                                el={el}
                                                centerAndZoom={centerAndZoomHandler}
                                                closeInfoBox={closeInfoBox}
                                            />
                                        </Infobox>
                                    )}
                                </React.Fragment>
                            ))}
                        </ClusterLayer>
                    </>
                )}
                {showPolygon && (
                    <Layer animationDuration={5000}>
                        <Pushpin
                            location={{
                                longitude: -118.41662684083538,
                                latitude: 32.795557149977014,
                            }}
                        />
                        <Polyline
                            path={[
                                {
                                    longitude: -118.41662684083538,
                                    latitude: 32.795557149977014,
                                },
                                {
                                    longitude: -118.41468349477175,
                                    latitude: 32.795636623484093,
                                },
                            ]}
                            curved={true}
                            withMovingMarker={true}
                            level={0}
                        />
                        <Polyline
                            path={[
                                {
                                    longitude: -118.41662684083538,
                                    latitude: 32.795557149977014,
                                },
                                {
                                    longitude: -118.41468349477175,
                                    latitude: 32.795636623484093,
                                },
                            ]}
                        />
                        <Pushpin
                            location={{
                                longitude: -118.41468349477175,
                                latitude: 32.795636623484093,
                            }}
                        />
                        <Polyline
                            strokeThickness={2}
                            strokeColor="red"
                            fillColor="blue"
                            path={polygon}
                            curved={true}
                        />
                        {/* 
            <Polygon
              path={polygon}
              fillColor={"yellow"}
              strokeColor={"orange"}
              strokeThickness={3}
              strokeDashArray={[1, 2, 5, 10]}
            /> */}
                    </Layer>
                )}
            </ReactBingMap>
        </div>
    );
};

export default MapBing3;
