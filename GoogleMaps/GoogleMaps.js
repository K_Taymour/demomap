import React, { Component } from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

export class MapContainer extends Component {
    onMarkerClick = (props, marker) => {
        // this.setState({
        //   activeMarker: marker,
        //   selectedPlace: props,
        //   showingInfoWindow: true
        // });
        alert('marker clicked!');
    }

    render() {
        return (
            <Map google={this.props.google} zoom={14}>

                <Marker onClick={this.onMarkerClick}
                    name={'Current location'} />

                <Marker
                    name="SOMA"
                    position={{ lat: 37.778519, lng: -122.40564 }}
                    title="The marker`s title will appear as a tooltip."
                />
                {/* <InfoWindow onClose={this.onInfoWindowClose}>
                    <div>
                        <h1>{this.state.selectedPlace.name}</h1>
                    </div>
                </InfoWindow> */}
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyDdjXRFN8l-HXi2OfLiDiN2FnWy2mhPuso")
})(MapContainer)