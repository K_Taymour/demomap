/* eslint-disable no-unused-vars */
import React, { useLayoutEffect, useRef } from 'react';

export const HereMap = () => {
  const mapRef = useRef(null);

  useLayoutEffect(() => {
    if (!mapRef.current) return;
    const { H } = window;
    const platform = new H.service.Platform({
      apikey: 'q3OKsZk6PeP4ufg0e6cAH8hz2DmYPCMNU7JQ5jeHkjk'
    });
    const defaultLayers = platform.createDefaultLayers();
    const hMap = new H.Map(mapRef.current, defaultLayers.vector.normal.map, {
      center: { lat: 40, lng: -110.5 },
      zoom: 4,
      pixelRatio: window.devicePixelRatio || 1
    });

    // var parisMarker = new H.map.Marker({ lat: -110.0, lng: 40 });
    // H.map.addObject(parisMarker);

    const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(hMap));

    const ui = H.ui.UI.createDefault(hMap, defaultLayers);

    // eslint - disable - next - line consistent -return
    return () => {
      hMap.dispose();
    };
  }, [mapRef]);

  return (
    <div
      className="map"
      ref={mapRef}
      style={{ height: '100%', width: '100%' }}
    />
  );
};

export default HereMap;

/////////////////////////////

// import React, { useRef, useState, useEffect } from 'react';
// import LAPoints from '../../data/laPoints.json';
// import missedCallPng from '../../assets/icons/missed-call.png';
// import ringingCallPng from '../../assets/icons/ringing-call.png';
// import "./DisplayMapFC.css";

// const HereMap = () => {
//   let H = useRef(null);
//   let myMap = useRef(null);
//   let myUI = useRef(null);
//   // Create a reference to the HTML element we want to put the map on
//   const mapRef = React.useRef(null);

//   React.useLayoutEffect(() => {
//     // `mapRef.current` will be `undefined` when this hook first runs; edge case that
//     if (!mapRef.current) return;
//     H = window.H;
//     const platform = new H.service.Platform({
//       apikey: "t0LxaY1li5xc-ZaqHFvl4O0s0ecqggdztTK7UubCAys"
//     });
//     const defaultLayers = platform.createDefaultLayers();
//     myMap.current = new H.Map(mapRef.current, defaultLayers.vector.normal.map, {
//       center: { lat: 33.91, lng: -118.23 },
//       zoom: 11,
//       pixelRatio: window.devicePixelRatio || 1
//     });

//     // MapEvents enables the event system
//     // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
//     const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(myMap.current));

//     // Create the default UI components
//     myUI = H.ui.UI.createDefault(myMap.current, defaultLayers);

//     // This will act as a cleanup to run once this hook runs again.
//     // This includes when the component un-mounts
//     return () => {
//       myMap.current.dispose();
//     };
//   }, [mapRef]); // This will run this hook every time this ref is updated

//   function handleAddManyPoints() {
//     H = window.H;
//     console.log(H)


//     let myMarker, myIcon, myMarkerPosition;

//     //#region customized icons
//     let missedCallIcon = new H.map.Icon(missedCallPng, { size: { w: 40, h: 40 } });
//     let ringingCallIcon = new H.map.Icon(ringingCallPng, { size: { w: 40, h: 40 } });

//     //#endregion


//     LAPoints.features.forEach(feat => {
//       let lng = feat.geometry.coordinates[0];
//       let lat = feat.geometry.coordinates[1];
//       let callState = feat.properties.callState;

//       if (callState === "ringing") {
//         myIcon = ringingCallIcon;
//       }
//       else if (callState === "missed") {
//         myIcon = missedCallIcon;
//       }

//       myMarker = new H.map.Marker({
//         lat: lat,
//         lng: lng
//       }, {
//         icon: myIcon
//       });
//       myMarker.setData(`<div>
//             <h3>${callState}</h3>
//             <button>Answer</button>
//             <div/>`);

//       myMarker.addEventListener("tap", e => {

//         //Firstly check if another bubble is open & remove it
//         myUI.getBubbles().forEach(bub => myUI.removeBubble(bub));

//         myMarkerPosition = new H.geo.Point(e.target.b.lat, e.target.b.lng);

//         let bubble = new H.ui.InfoBubble(
//           myMarkerPosition,
//           {
//             content: e.target.data
//           }
//         );
//         bubble.addClass("markerBubble");
//         myUI.addBubble(bubble);
//       }, false);

//       myMap.current.addObject(myMarker);
//     });
//   }

//   let CUSTOM_THEME = {
//     getClusterPresentation: function (cluster) {
//       // Get random DataPoint from our cluster
//       // var randomDataPoint = getRandomDataPoint(cluster),
//       // Get a reference to data object that DataPoint holds
//       let data = cluster;

//       // Create a marker from a random point in the cluster
//       var clusterMarker = new H.map.Marker(cluster.getPosition(), {
//         icon: new H.map.Icon(missedCallPng, { size: { w: 40, h: 40 } }),

//         // Set min/max zoom with values from the cluster,
//         // otherwise clusters will be shown at all zoom levels:
//         min: cluster.getMinZoom(),
//         max: cluster.getMaxZoom()
//       });

//       // Link data from the random point from the cluster to the marker,
//       // to make it accessible inside onMarkerClick
//       clusterMarker.setData(data)
//       // Show a bubble on marker click/tap
//       // .addEventListener('tap', onMarkerClick);

//       return clusterMarker;
//     },
//     getNoisePresentation: function (noisePoint) {
//       // Get a reference to data object our noise points
//       let ringingCallElement = document.createElement('img');
//       ringingCallElement.src = ringingCallPng;
//       ringingCallElement.classList.add('callIcon');

//       var data = noisePoint.getData(),
//         // Create a marker for the noisePoint
//         noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
//           // Use min zoom from a noise point
//           // to show it correctly at certain zoom levels:
//           min: noisePoint.getMinZoom(),

//           icon: new H.map.DomIcon(ringingCallElement)

//         });

//       // Link a data from the point to the marker
//       // to make it accessible inside onMarkerClick
//       noiseMarker.setData(data);

//       // Show a bubble on marker click/tap
//       // noiseMarker.addEventListener('tap', onMarkerClick);

//       return noiseMarker;
//     }
//   };

//   function handleAddManyPointsClusterd() {
//     H = window.H;
//     console.log(H)

//     //create an array of DataPoint objects,
//     // for the ClusterProvider
//     let dataPoints = LAPoints.features.map(function (feat) {
//       return new H.clustering.DataPoint(feat.geometry.coordinates[1], feat.geometry.coordinates[0]);
//     });

//     // Create a clustering provider with custom options for clusterizing the input
//     let clusteredDataProvider = new H.clustering.Provider(dataPoints, {
//       clusteringOptions: {
//         // Maximum radius of the neighbourhood
//         eps: 32,
//         // minimum weight of points required to form a cluster
//         minWeight: 2
//       },
//       // theme: CUSTOM_THEME
//     });

//     // Create a layer tha will consume objects from our clustering provider
//     let clusteringLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);

//     //add our layer to the map
//     myMap.current.addLayer(clusteringLayer);
//   }

//   function handleAddManyPointsDOMIcon() {
//     H = window.H;

//     let myIcon, myMarker;

//     let missedCallElement = document.createElement('img');
//     missedCallElement.src = missedCallPng;
//     missedCallElement.classList.add('callIcon');

//     let ringingCallElement = document.createElement('img');
//     ringingCallElement.src = ringingCallPng;
//     ringingCallElement.classList.add('callIcon');

//     LAPoints.features.forEach(feat => {
//       let lng = feat.geometry.coordinates[0];
//       let lat = feat.geometry.coordinates[1];
//       let callState = feat.properties.callState;
//       let redCircle = feat.properties.redCircle;
//       let myMarkerPosition;

//       if (callState === "ringing") {
//         if (redCircle) ringingCallElement.classList.add('redCircle');
//         else ringingCallElement.classList.remove('redCircle');

//         myIcon = new H.map.DomIcon(ringingCallElement);
//       }

//       else if (callState === "missed") {
//         if (redCircle) missedCallElement.classList.add('redCircle');
//         else missedCallElement.classList.remove('redCircle');

//         myIcon = new H.map.DomIcon(missedCallElement);
//       }

//       myMarker = new H.map.DomMarker({
//         lat: lat,
//         lng: lng
//       }, {
//         icon: myIcon
//       });

//       myMarker.setData(`<div>
//             <h3>${callState}</h3>
//             <button>Answer</button>
//             <div/>`);

//       myMarker.addEventListener("tap", e => {

//         //Firstly check if another bubble is open & remove it
//         myUI.getBubbles().forEach(bub => myUI.removeBubble(bub));

//         myMarkerPosition = new H.geo.Point(e.target.b.lat, e.target.b.lng);

//         let bubble = new H.ui.InfoBubble(
//           myMarkerPosition,
//           {
//             content: e.target.data
//           }
//         );
//         bubble.addClass("markerBubble");
//         myUI.addBubble(bubble);
//       }, false);

//       myMap.current.addObject(myMarker);

//     });


//   }

//   function handleAddLabels() {
//     H = window.H;

//     let myIcon, myMarker, callElement;

//     LAPoints.features.forEach(feat => {
//       let lng = feat.geometry.coordinates[0];
//       let lat = feat.geometry.coordinates[1];
//       let redCircle = feat.properties.redCircle;
//       let callState = feat.properties.callState;

//       callElement = `<div class="labelOnHereMap"> ${callState} <div/>`;

//       myIcon = new H.map.DomIcon(callElement);

//       myMarker = new H.map.DomMarker({
//         lat: lat,
//         lng: lng
//       }, {
//         icon: myIcon
//       });

//       myMap.current.addObject(myMarker);
//     })
//   }
//   // function handleDeleteMarkers() {
//   //     myMap.removeObjects(myMap.getObjects ())
//   // }

//   return <>
//     <button onClick={handleAddManyPoints}>add many points</button>
//     <button onClick={handleAddManyPointsClusterd}>add many points [Clusterd]</button>
//     <button onClick={handleAddManyPointsDOMIcon}>add many points [DOM Icon]</button>
//     <button onClick={handleAddLabels}>add Labels</button>
//     {/* <button onClick={handleDeleteMarkers}>Delete Markers</button> */}

//     <div className="map" ref={mapRef} style={{ height: "500px" }} />
//   </>;
// };

// export default HereMap;

/////////////////

// import React, { useRef, memo } from 'react';
// import { HEREMap, Marker } from 'here-maps-react';
// import missedCallPng from '../../assets/icons/missed-call.png';
// import ringingCallPng from '../../assets/icons/ringing-call.png';
// import LAPoints from '../../data/laPoints.json';
// import ErrorBoundary from './ErrorBoundary';

// const HereMap = () => {
//   const mapRef = useRef();
//   let myMap = useRef(null);
//   let myUI = useRef(null);

//   const handleTap = (e) => {
//     console.log(mapRef)
//     console.log(window.H)
//     console.log('e', e);

//     let button = document.createElement('button');
//     button.innerHTML = 'click me';
//     // where do we want to have the button to appear?
//     // you can append it to another element just by doing something like
//     document.getElementById('map-container').appendChild(button);
//     // document.body.appendChild(button);
//   }

//   const myMarkers = LAPoints.features.map((feat, index) => {

//     if (index < 10) {
//       if (feat.geometry.type === "Point") {
//         let lng = feat.geometry.coordinates[0];
//         let lat = feat.geometry.coordinates[1];
//         let callState = feat.properties.callState;
//         let redCircle = feat.properties.redCircle;
//         let myMarkerPosition;
//         let classesList = 'callIcon';

//         if (index <= 4) {
//           if (callState === "ringing") {
//             if (redCircle) classesList = 'callIcon redCircle';
//             else classesList = 'callIcon';

//             return (
//               <Marker
//                 key={index}
//                 lat={lat}
//                 lng={lng}
//                 children={<img src={ringingCallPng} className={classesList} />}
//                 onTap={handleTap}
//               >
//               </Marker>
//             )
//           }

//           else if (callState === "missed") {
//             if (redCircle) classesList = 'callIcon redCircle';
//             else classesList = 'callIcon';

//             return (
//               <Marker
//                 key={index}
//                 lat={lat}
//                 lng={lng}
//                 children={<img src={missedCallPng} className={classesList} />}
//                 onTap={handleTap}
//               >
//               </Marker>
//             )
//           }
//         }
//         else {
//           return (
//             <Marker
//               key={index}
//               lat={lat}
//               lng={lng}
//               children={<div className="labelOnMap"> ${callState} </div>}
//               onTap={handleTap}
//             >
//             </Marker>
//           )
//         }
//       }
//     }

//     else return null;
//   });


//   return (
//     // <ErrorBoundary>
//     <div style={{ height: '100%', width: '100%' }}>
//       <HEREMap
//         // ref={mapRef}
//         apikey="ICUjdk2iphEDQYyb8ymusoTGcRJpL-6kjTbepQoLm90"
//         interactive={true}
//         center={{ lat: 33.91, lng: -118.23 }}
//         zoom={11}
//         style={{ height: "100%" }}
//       >

//         {myMarkers}
//         {/* <Marker
//           lat={33.91}
//           lng={-118.23}
//         />

//         <Marker
//           lat={10.998666}
//           lng={-63.79841}
//         /> */}

//       </HEREMap>
//     </div>

//     // </ErrorBoundary>
//   )
// }

// export default HereMap;