import React, { useState } from "react";
import GoogleInfoWindow from "./GoogleInfoWindow";

const GoogleMapsLabel = (props) => {

    const [infoWindowStatus, setInfoWindowStatus] = useState(false);

    const toggleInfoWindow = () => {
        setInfoWindowStatus(!infoWindowStatus);
    }

    const fontSize = props.fontSize || props.height || 15;
    const iconWidth = props.width || props.text.length * fontSize * 0.35 || 60;
    const iconHeight = props.height || fontSize + 2;

    let labelStyle = {
        // Change the graphic icon origin from (top-left) to (center) of the location
        position: "absolute",
        width: iconWidth,
        height: iconHeight,
        transform: "translate(-50%, -50%)",
        // left: -iconWidth / 2,
        // top: -iconHeight / 2,

        // Label styles
        backgroundColor: "black",
        borderRadius: "2px",
        boxShadow: "2px 2px 2px 2px black"
    };

    const textStyle = {
        color: "white",
        textAlign: "center",
        marginTop: 3
    }

    if (props.$hover) {
        labelStyle.cursor = "pointer";
    }

    return (<>
        <div style={labelStyle} onClick={toggleInfoWindow}>
            <div style={textStyle}>{props.text}</div>
        </div>
        {infoWindowStatus && <GoogleInfoWindow onCloseInfoWindow={toggleInfoWindow} />}
    </>);
}

export default GoogleMapsLabel;