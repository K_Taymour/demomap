import React, { useRef, useState } from 'react';
import GoogleMapReact from 'google-map-react';
import GoogleMapsIcon from './GoogleMapsIcon';
import GoogleMapsLabel from './GoogleMapsLabel';
// import MarkerClusterer from '@googlemaps/markerclustererplus';
import { markersData, centerLocation } from './fakeData';


export default function GoogleMap() {
  const mapRef = useRef();

  const createCluster = (map, maps) => {
    const markers = locations.map((location, i) => {
      let marker = new maps.Marker({
        position: location,
        map,
        label: "A",
      });
      // marker.setMap(map);
      return marker;
    });
    // Add a marker clusterer to manage the markers.
    // new MarkerClusterer(map, markers, {
    //   imagePath:
    //     "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
    //   maxZoom: 7
    // });
  }

  const createDefaultInfoWindow = (map, maps) => {
    const contentString =
      '<div id="content">' +
      '<div id="siteNotice">' +
      "</div>" +
      '<h3 id="firstHeading" class="firstHeading">Info Window</h3>' +
      '<div id="bodyContent">' +
      "<p>This is a sample of Default InfoWindow</p>" +
      "</div>" +
      "</div>";
    const infowindow = new maps.InfoWindow({
      content: contentString,
    });

    const marker = new maps.Marker({
      position: location,
      map,
      title: "Uluru (Ayers Rock)",
    });
    marker.addListener("click", () => {
      infowindow.open(map, marker);
    });
  }

  const locations = [
    { lat: -31.56391, lng: 147.154312 },
    { lat: -33.718234, lng: 150.363181 },
    { lat: -33.727111, lng: 150.371124 },
    { lat: -33.848588, lng: 151.209834 },
    { lat: -33.851702, lng: 151.216968 },
    { lat: -34.671264, lng: 150.863657 },
    { lat: -35.304724, lng: 148.662905 },
    { lat: -36.817685, lng: 175.699196 },
    { lat: -36.828611, lng: 175.790222 },
    { lat: -37.75, lng: 145.116667 },
    { lat: -37.759859, lng: 145.128708 },
    { lat: -37.765015, lng: 145.133858 },
    { lat: -37.770104, lng: 145.143299 },
    { lat: -37.7737, lng: 145.145187 },
    { lat: -37.774785, lng: 145.137978 },
    { lat: -37.819616, lng: 144.968119 },
    { lat: -38.330766, lng: 144.695692 },
    { lat: -39.927193, lng: 175.053218 },
    { lat: -41.330162, lng: 174.865694 },
    { lat: -42.734358, lng: 147.439506 },
    { lat: -42.734358, lng: 147.501315 },
    { lat: -42.735258, lng: 147.438 },
    { lat: -43.999792, lng: 170.463352 },
  ];


  let defaultGraphics = [
    {
      type: "icon",
      lat: -31.56391,
      lng: 147.154312,
      icon: "Call.png"
    },
    {
      type: "icon",
      lat: -33.718234,
      lng: 150.363181,
      icon: "Call.png",
      isIdle: true
    },
    {
      type: "label",
      lat: -33.848588,
      lng: 151.209834,
      text: "Label",
    },
    {
      type: "label",
      lat: -33.727111,
      lng: 150.371124,
      text: "Label for Testing",
    },
    {
      type: "label",
      lat: -33.851702,
      lng: 151.216968,
      text: "Another Label for Testing",
    }
  ];


  const [mapGraphics, setMapGraphics] = useState(defaultGraphics);
  const [zoom, setZoom] = useState(13);
  const [bounds, setBounds] = useState();

  const location = { lat: 45.4, lng: -75.6 };

  const handleApiLoaded = (map, maps) => {
    console.log(map, maps);
    // createDefaultInfoWindow(maps, maps);
    // createCluster(map, maps);

    map.addListener("zoom_changed", () => {
      console.log(map.getZoom());
      // if (map.getZoom() < 10) {
      //   setMapGraphics([]);
      // }
    });
  };



  const getMapOptions = (maps) => {
    return {
      streetViewControl: false,
      scaleControl: true,
      fullscreenControl: false,
      styles: [{
        featureType: "poi.business",
        elementType: "labels",
        stylers: [{
          visibility: "off"
        }]
      }],
      gestureHandling: "greedy",
      disableDoubleClickZoom: true,
      minZoom: 11,
      maxZoom: 18,

      mapTypeControl: true,
      mapTypeId: maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {
        style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: maps.ControlPosition.BOTTOM_CENTER,
        mapTypeIds: [
          maps.MapTypeId.ROADMAP,
          maps.MapTypeId.SATELLITE,
        ]
      },

      zoomControl: true,
      clickableIcons: false
    };
  }

  const onMarkerClicked = (key, childProps) => {
    console.log(key, childProps);
  }

  return (
    <div style={{ height: '100%', width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyDdjXRFN8l-HXi2OfLiDiN2FnWy2mhPuso' }}
        defaultCenter={centerLocation}
        defaultZoom={8}
        ref={mapRef}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
        // options={getMapOptions}
        onChildClick={onMarkerClicked}
      >
        {markersData.map(marker => {
          return (
            <GoogleMapsIcon
              key={marker.id}
              lat={marker.lat}
              lng={marker.lng}
              icon={'Call.png'}
            />
          );
        })}

        {mapGraphics.map(graphic => {
          switch (graphic.type) {
            case 'icon':
              return <GoogleMapsIcon key={Math.random() * 100} {...graphic} />;
            case 'label':
              return <GoogleMapsLabel key={Math.random() * 100} {...graphic} />;
          }
        })}

        <GoogleMapsLabel lat={centerLocation.lat} lng={centerLocation.lng} text={"Esri Northeast Africa"} />

        {/* <GoogleMapsIcon lat={location.lat} lng={location.lng + 0.01} isIdle={true} icon={"Call.png"} /> */}
        {/* <GoogleMapsLabel lat={location.lat + 0.01} lng={location.lng - 0.02} text="Label" /> */}
      </GoogleMapReact>
    </div>
  );
};