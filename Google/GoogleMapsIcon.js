import React from "react";

const GoogleMapsIcon = (props) => {
    const { $hover, isIdle, icon } = { ...props };
    const iconSize = 30;
    const iconWidth = iconSize;
    const iconHeight = iconWidth;

    let style = {
        // Change the graphic icon origin from (top-left) to (center) of the location
        position: "absolute",
        width: iconWidth,
        height: iconHeight,
        transform: "translate(-50%, -50%)",
        // left: -iconWidth / 2,
        // top: -iconHeight / 2,

        // Icon styles
        background: `url('/${icon}') no-repeat center`,
        backgroundSize: iconSize,
    };

    if (isIdle) {
        const idleStyle = {
            backgroundSize: iconSize - 5,
            border: "6px double red",
            borderRadius: "50%",
        }
        style = { ...style, ...idleStyle }
    }

    if (isIdle && $hover) {
        const hoverStyle = {
            backgroundSize: iconSize - 5,
            border: "6px solid green",
            borderRadius: "50%",
            cursor: "pointer",
        }
        style = { ...style, ...hoverStyle }
    }

    return <div style={style}></div>
}

export default GoogleMapsIcon;