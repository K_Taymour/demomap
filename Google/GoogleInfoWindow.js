import React from 'react'

export default function GoogleInfoWindow(props) {
    const fontSize = 15;
    const iconWidth = 200;
    const iconHeight = fontSize;

    return (
        <div style={{
            // Change the graphic icon origin from (top-left) to (center) of the location
            position: "absolute",
            width: iconWidth,
            height: "auto",
            transform: "translate(-50%, -150%)",
            // left: -iconWidth / 2,
            // bottom: 15,
            // top: -iconHeight * 2,
            zIndex: 2,
            backgroundColor: "white",
            borderRadius: 4,
            padding: 5,
            boxShadow: "black 0 0 10px 0px",
            textAlign: "center"
        }}>
            <span >This is sample of Info Window</span>
            <button style={{ marginLeft: 5 }} onClick={props.onCloseInfoWindow}>x</button>
        </div>
    )
}
