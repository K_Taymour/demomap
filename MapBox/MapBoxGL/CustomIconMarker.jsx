import React, { Component } from 'react';
import { Marker } from 'react-mapbox-gl';

import missedCall from '../../../assets/icons/missed-call.png';
import ringingCall from '../../../assets/icons/ringing-call.png';

export default class CustomIconMarker extends Component {
    render() {
        const { lat, lng, callState, redCircle } = this.props;

        let imageSrc;
        switch (callState) {
            case 'ringing':
                imageSrc = ringingCall;
                break;
            case 'missed':
                imageSrc = missedCall;
                break;
            default:
                break;
        }

        let markerStyle = {};

        switch (redCircle) {
            case true:
                markerStyle.border = '5px double red';
                markerStyle.borderRadius = '50%';
                break;
            case false:
                break;
            default:
                break;
        }

        return (
            <Marker
                coordinates={[lng, lat]}
                anchor="bottom">
                <img src={imageSrc} style={markerStyle} />
            </Marker>
        )
    }
}
