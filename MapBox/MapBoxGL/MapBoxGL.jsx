import React, { Component } from 'react';

import ReactMapboxGl, { Layer, Feature, ZoomControl, ScaleControl, Marker, Cluster } from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

import LabelMarker from './LabelMarker';
import CustomIconMarker from './CustomIconMarker';
import MyPopup from './MyPopup';
import places from '../../../data/laPoints.json';
import missedCall from '../../../assets/icons/missed-call.png';

const Map = ReactMapboxGl({
    accessToken:
        'pk.eyJ1IjoibWFjb25kb2pvaG4iLCJhIjoiY2tmd2dsZ2NsMWNwODJzbHI5djQ1dmoxcSJ9.NrQBbX23wBwK2SelE0OASg'
});

export default class MapBoxGL extends Component {
    render() {
        let lng = -0.44;
        let lat = 51.52;

        let clusterMarker = () => (
            <Marker coordinates={[lng, lat]} >
                c
            </Marker>
        );

        const polygonPaint = {
            'fill-color': '#6F788A',
            'fill-opacity': 0.7
        };

        const lineStyle = {
            'line-color': 'red',
            'line-width': 5
        }

        const AllShapesPolygonCoords = [
            [
                [
                    -0.16582489013671875,
                    51.52241608253253
                ],
                [
                    -0.16376495361328125,
                    51.48715542051101
                ],
                [
                    -0.0720977783203125,
                    51.4873692036782
                ],
                [
                    -0.07415771484375,
                    51.524979430024345
                ],
                [
                    -0.16582489013671875,
                    51.52241608253253
                ]
            ]
        ];

        const lineStringCoordinates = [
            [
                -0.06969451904296875,
                51.522202463728135
            ],
            [
                -0.16307830810546875,
                51.475823478237366
            ]
        ];

        return (
            <Map
                style="mapbox://styles/mapbox/streets-v11"
                zoom={[8]}
                containerStyle={{
                    height: '100%',
                    width: '100%'
                }}
            >
                <ZoomControl />
                <ScaleControl />

                <Layer type="symbol" id="marker" layout={{ 'icon-image': 'marker-15' }}>
                    <Feature coordinates={[-0.481747846041145, 51.3233379650232]} />
                </Layer>

                <Layer type="fill" paint={polygonPaint}>
                    <Feature coordinates={AllShapesPolygonCoords} />
                </Layer>

                <Layer type="line" paint={lineStyle}>
                    <Feature coordinates={lineStringCoordinates} />
                </Layer>

                <LabelMarker lat={51.52} lng={-0.44} />

                <CustomIconMarker lat={51.52} lng={-0.24} callState='ringing' redCircle={true} />
                <CustomIconMarker lat={51.32} lng={-0.24} callState='ringing' redCircle={false} />
                <CustomIconMarker lat={51.22} lng={-0.24} callState='missed' redCircle={true} />
                <CustomIconMarker lat={51.22} lng={-0.44} callState='missed' redCircle={false} />

                <Cluster ClusterMarkerFactory={clusterMarker}>
                    {
                        places.features.map((feature, key) =>
                            <Marker
                                key={key}
                                coordinates={feature.geometry.coordinates}
                            >
                                {console.log(feature, key)}
                                <img src={missedCall} />
                            </Marker>
                        )
                    }
                </Cluster>


                {/* <MyPopup lat={51.52} lng={-0.24} /> */}

                {/* <MyCluster /> */}
            </Map>
        )
    }
}
