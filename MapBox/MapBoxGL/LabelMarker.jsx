import React, { Component } from 'react';
import { Marker } from 'react-mapbox-gl';
import './LabelMarker.css'; 

export default class LabelMarker extends Component {
    render() {
        const { lat, lng } = this.props;

        return (
            <Marker
                coordinates={[lng, lat]}
                anchor="center">
                <div className='label-marker'>
                    label marker
                </div>
            </Marker>
        )
    }
}
