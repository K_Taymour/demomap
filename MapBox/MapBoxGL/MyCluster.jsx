// import React, { Component } from 'react';
// import { Cluster } from "react-mapbox-gl";
// import places from '../data/laPoints.json'

// export default class MyCluster extends Component {
//     render() {
//         const styles = {
//             clusterMarker: {},
//             marker: {}
//         }

//         const clusterMarker = (coordinates) => (
//             <Marker coordinates={coordinates} style={styles.clusterMarker}>
//                 C
//             </Marker>
//         );


//         return (
//             <Cluster ClusterMarkerFactory={clusterMarker}>
//                 {
//                     places.features.map((feature, key) =>
//                         <Marker
//                             key={key}
//                             style={styles.marker}
//                             coordinates={feature.geometry.coordinates}
//                             onClick={this.onMarkerClick.bind(this, feature.geometry.coordinates)}>
//                             M
//                         </Marker>
//                     )
//                 }
//             </Cluster>
//         )
//     }
// }
