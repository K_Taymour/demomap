import React, { Component } from 'react';
import { Popup } from "react-mapbox-gl";

export default class MyPopup extends Component {
    render() {
        const { lat, lng } = this.props;

        return (
            <Popup
                coordinates={[lng, lat]}
                offset={{
                    'bottom-left': [12, -38], 'bottom': [0, -38], 'bottom-right': [-12, -38]
                }}>
                <div>
                    <h1>Popup</h1>
                    <div>body</div>
                    <button>click</button>
                </div>
            </Popup>
        )
    }
}
