/* global document */
import * as React from 'react';
import { useState } from 'react';
import { render } from 'react-dom';

import MapGL, { NavigationControl, setRTLTextPlugin } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

import LabelMarker from './LabelMarker';
import CustomIconMarker from './CustomIconMarker';
import MyPopup from './MyPopup'

const MAPBOX_TOKEN = 'pk.eyJ1IjoibWFjb25kb2pvaG4iLCJhIjoiY2tmd2dsZ2NsMWNwODJzbHI5djQ1dmoxcSJ9.NrQBbX23wBwK2SelE0OASg'; // Set your mapbox token here

export default function UberMapGL() {
    const [viewport, setViewport] = useState({
        latitude: 34.05,
        longitude: -118.24,
        zoom: 10,
        bearing: 0,
        pitch: 0
    });

    return (
        <MapGL
            {...viewport}
            width="100%"
            height="100%"
            mapStyle="mapbox://styles/mapbox/streets-v11"
            onViewportChange={nextViewport => setViewport(nextViewport)}
            mapboxApiAccessToken={MAPBOX_TOKEN}
        >

            <div style={{ position: 'absolute', left: 0 }}>
                <NavigationControl />
            </div>

            <LabelMarker longitude={-118.24} latitude={34.05} />

            <CustomIconMarker longitude={-118.44} latitude={34.05} callState='ringing' redCircle={true} />
            <CustomIconMarker longitude={-118.64} latitude={34.05} callState='ringing' redCircle={false} />
            <CustomIconMarker longitude={-118.04} latitude={34.05} callState='missed' redCircle={true} />
            <CustomIconMarker longitude={-118.04} latitude={33.95} callState='missed' redCircle={false} />

            <MyPopup />
        </MapGL>
    );
}