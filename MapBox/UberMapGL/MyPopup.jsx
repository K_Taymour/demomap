import { Popup } from 'react-map-gl';
import React, { Component } from 'react'

export default class MyPopup extends Component {
    render() {
        return (
            <Popup
                latitude={34}
                longitude={-118}
                closeButton={true}
                closeOnClick={false}
                // onClose={() => this.setState({showPopup: false})}
                anchor="top" >
                <div>You are here</div>
            </Popup>
        )
    }
}
