import * as React from 'react';
import { BaseControl, Popup } from 'react-map-gl';
import missedCall from '../../../assets/icons/missed-call.png';
import ringingCall from '../../../assets/icons/ringing-call.png';

import MyPopup from './MyPopup';

class CustomIconMarker extends BaseControl {
    state = {
        showPopup: false
    };

    handleMarkerClick = () => {
        this.setState({
            showPopup: true
        });

        return (
            <Popup
                latitude={34}
                longitude={-118}
                closeButton={true}
                closeOnClick={false}
                // onClose={() => this.setState({showPopup: false})}
                anchor="top" >
                <div>You are here</div>
            </Popup>
        )

    };

    _render() {
        const { longitude, latitude, callState, redCircle } = this.props;

        const [x, y] = this._context.viewport.project([longitude, latitude]);

        const markerStyle = {
            position: 'absolute',
            left: x,
            top: y,
        };

        let imageSrc;
        switch (callState) {
            case 'ringing':
                imageSrc = ringingCall;
                break;
            case 'missed':
                imageSrc = missedCall;
                break;
            default:
                break;
        }

        switch (redCircle) {
            case true:
                markerStyle.border = '5px double red';
                markerStyle.borderRadius = '50%';
                break;
            case false:
                break;
            default:
                break;
        }

        return (
            <div>
                <img ref={this._containerRef} src={imageSrc} style={markerStyle} onClick={this.handleMarkerClick} />
                <MyPopup />
            </div>
        );
    }
}

export default CustomIconMarker;