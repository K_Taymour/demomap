// import React, { useRef, useEffect, useState } from 'react';
// import mapboxgl from 'mapbox-gl';
// import './MapBox.css';

// mapboxgl.accessToken =
//   'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';

// const MapBox = () => {
//   const mapContainerRef = useRef(null);

//   const [lng, setLng] = useState(5);
//   const [lat, setLat] = useState(34);
//   const [zoom, setZoom] = useState(1.5);

//   // Initialize map when component mounts
//   useEffect(() => {
//     const map = new mapboxgl.Map({
//       container: mapContainerRef.current,
//       style: 'mapbox://styles/mapbox/streets-v11',
//       center: [lng, lat],
//       zoom: zoom
//     });

//     // Add navigation control (the +/- zoom buttons)
//     map.addControl(new mapboxgl.NavigationControl(), 'top-right');

//     map.on('move', () => {
//       setLng(map.getCenter().lng.toFixed(4));
//       setLat(map.getCenter().lat.toFixed(4));
//       setZoom(map.getZoom().toFixed(2));
//     });

//     // Clean up on unmount
//     return () => map.remove();
//   }, []); // eslint-disable-line react-hooks/exhaustive-deps

//   return (
//     <div>
//       <div className='sidebarStyle'>
//         <div>
//           Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
//         </div>
//       </div>
//       <div className='map-container' ref={mapContainerRef} />
//     </div>
//   );
// };

// export default MapBox;


import React, { useRef, useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import './MapBox.css';
import ringingCall from '../../../assets/icons/ringing-call.png'

mapboxgl.accessToken =
  'pk.eyJ1IjoibWFjb25kb2pvaG4iLCJhIjoiY2tmd2dsZ2NsMWNwODJzbHI5djQ1dmoxcSJ9.NrQBbX23wBwK2SelE0OASg';

const MapBox = () => {
  const mapContainerRef = useRef(null);

  const [lng, setLng] = useState(-110.5);
  const [lat, setLat] = useState(34);
  const [zoom, setZoom] = useState(4);

  // Initialize map when component mounts
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });

    // Add navigation control (the +/- zoom buttons)
    map.addControl(new mapboxgl.NavigationControl(), 'top-right');

    let el = document.createElement('div');
    el.className = 'marker';
    el.style.backgroundImage = 'Call.png';
    el.style.width = '40px';
    el.style.height = '40px';
    el.style.border = '5px double red';
    el.style.borderRadius = '50%';

    // create the popup
    let popup = new mapboxgl.Popup({ offset: 25 })
      .setHTML('<h3> h3333 </h3><p>pppp</p><button>click</button>') // CHANGE THIS TO REFLECT THE PROPERTIES YOU WANT TO SHOW

    let marker = new mapboxgl.Marker(el)
      .setLngLat([-110.5, 30.5])
      .setPopup(popup) // sets a popup on this marker
      .addTo(map);

    let el2 = document.createElement('div');
    el2.innerText = 'am text';
    el2.style.backgroundColor = 'white';
    el2.style.border = '1px solid black';
    let marker2 = new mapboxgl.Marker(el2)
      .setLngLat([-110.5, 40.5])
      .addTo(map);



    map.on('move', () => {
      setLng(map.getCenter().lng.toFixed(4));
      setLat(map.getCenter().lat.toFixed(4));
      setZoom(map.getZoom().toFixed(2));
    });

    // Clean up on unmount
    return () => map.remove();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <div className='sidebarStyle'>
        <div>
          Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
        </div>
      </div>
      <div className='map-container' ref={mapContainerRef} />
    </div>
  );
};

export default MapBox;