// import React, { useRef } from 'react';
// import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
// import 'leaflet/dist/leaflet.css';

// const LeafletMap = () => {
//   const mapRef = useRef();

//   return (
//     <Map
//       ref={mapRef}
//       center={[45.4, -75.7]}
//       zoom={13}
//       style={{
//         height: '100%',
//         width: '100%'
//       }}
//     >
//       <TileLayer
//         url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
//         attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
//       />
//       <Marker position={[45.4, -75.7]}>
//         <Popup>
//           A pretty CSS3 popup.
//           <br />
//           Easily customizable.
//         </Popup>
//       </Marker>
//     </Map>
//   );
// };

// export default LeafletMap;



import React, { Component, useEffect, useRef } from 'react';
import { Map, TileLayer, Marker, Tooltip, Popup } from 'react-leaflet';
import L, { marker } from 'leaflet';
import MarkerClusterGroup from "react-leaflet-markercluster";

import missedCallPng from '../../assets/icons/missed-call.png';
import ringingCallPng from '../../assets/icons/ringing-call.png';
import shadowPng from '../../assets/icons/shadow.png';
import locations from '../../data/locations.json';
import LAPoints from '../../data/laPoints.json';
import 'leaflet/dist/leaflet.css';
import 'react-leaflet-markercluster/dist/styles.min.css';
import './ReactLeaflet.css';

import ReactLeafletPopup from './ReactLeafletPopup'

const LeafletMap = () => {
  const mapRef = useRef();

  //Before anything => show the default icons
  useEffect(() => {
    delete L.Icon.Default.prototype._getIconUrl;

    L.Icon.Default.mergeOptions({
      iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
      iconUrl: require("leaflet/dist/images/marker-icon.png"),
      shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
    });
  }, [])

  // adds icons once the map initialized
  useEffect(() => {
    //#region customizing icons
    // const iconsOptions = {
    //     iconUrl: "",
    //     shadowUrl: shadowPng,
    //     iconSize: [40, 40], // size of the icon
    //     shadowSize: [40, 30], // size of the shadow
    //     iconAnchor: [20, 20], // point of the icon which will correspond to marker's location
    //     shadowAnchor: [10, 10],  // the same for the shadow
    //     popupAnchor: [-15, 0] // point from which the popup should open relative to the iconAnchor
    // };

    // iconsOptions.iconUrl = missedCallPng;
    // const missedCall = L.icon(iconsOptions);

    // iconsOptions.iconUrl = ringingCallPng;
    // const ringingCall = L.icon(iconsOptions);
    // //#endregion

    // const { current } = mapRef;
    // const { leafletElement: map } = current;

    // if (!map) return;

    // const myMarker = L.marker([33.91, -118.23], { icon: missedCall });
    // const myMarker2 = L.marker([33.91, -118.13], { icon: ringingCall });
    // //Customizable icon by html
    // const myMarker3 = L.marker([33.91, -118.03], {
    //     icon:
    //         L.divIcon({
    //             html: '<div class="location-marker"> hi </div>',
    //             iconSize: [26, 26]
    //         })
    // });

    // myMarker.bindPopup('popup');
    // myMarker2.bindPopup('popup2');
    // myMarker.bindTooltip('tooltip');
    // myMarker2.bindTooltip('tooltip2');
    // myMarker.addTo(map);
    // myMarker2.addTo(map);
    // myMarker3.addTo(map);

    // const myGeojson = new L.GeoJSON(locations, {
    //     onEachFeature: (feature = {}, layer) => {
    //         const { properties = {} } = feature;
    //         const { name, website } = properties;

    //         const myPopup = L.popup();
    //         const myHtmlTemplate = `
    //             <div>
    //                 <h3>${name}</h3>
    //                 <h3><a href="${website}"> ${website} </a></h3>
    //             </div>
    //         `;
    //         // const myHtmlTemplate = <Popup name={name} website={website} />
    //         myPopup.setContent(myHtmlTemplate);

    //         layer.bindPopup(myPopup);
    //     },
    // });
    // myGeojson.addTo(map);

  }, [mapRef]);

  function handleAddCircle() {
    const { current } = mapRef;
    const { leafletElement: map } = current;

    map.locate({
      setView: true
    })
  };

  useEffect(() => {
    const { current } = mapRef;
    const { leafletElement: map } = current;

    if (!map) return;

    map.on('locationfound', handleOnLocationFound)
  }, []);

  function handleOnLocationFound(e) {
    // console.log(e);
    const { current } = mapRef;
    const { leafletElement: map } = current;
    const latlng = e.latlng;

    const marker = L.marker(latlng);
    marker.addTo(map);

    const radius = e.accuracy;
    const accCircle = L.circle(latlng, {
      radius
    });
    accCircle.addTo(map);
  }

  function handleAddManyPoints() {
    //////
    let myIcon;
    const iconsOptions = {
      iconUrl: "",
      shadowUrl: shadowPng,
      iconSize: [40, 40], // size of the icon
      iconAnchor: [20, 20], // point of the icon which will correspond to marker's location
      popupAnchor: [-15, 0] // point from which the popup should open relative to the iconAnchor
    };

    iconsOptions.iconUrl = missedCallPng;
    const missedCallIcon = L.icon(iconsOptions);
    iconsOptions.iconUrl = ringingCallPng;
    const ringingCallIcon = L.icon(iconsOptions);
    //////

    const { current } = mapRef;
    const { leafletElement: map } = current;

    let markersClusterGroup = L.markerClusterGroup();
    let LAGeojson = new L.GeoJSON(LAPoints, {
      onEachFeature: (feature = {}, layer) => {
        function handleClickPopup(e) {
          // console.log(e);
        };

        const myPopup = L.popup();
        const myHtmlTemplate = `
                        <div>
                            <h3> a point </h3>
                            <button onClick="handleClickPopup()">Answer</button>                        
                        </div>
                    `;
        // const myHtmlTemplate = <Popup name={name} website={website} />
        myPopup.setContent(myHtmlTemplate);
        layer.bindPopup(myPopup);
      },
      pointToLayer: function (feature = {}, latlng) {
        const { properties = {} } = feature;
        const { callState } = properties;
        const { redCircle } = properties;
        let myClassName;

        if (callState === "ringing") {
          if (redCircle) myClassName = 'redCircle';
          else myClassName = null;

          myIcon = L.icon({
            iconUrl: ringingCallPng,
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            popupAnchor: [-15, 0],
            className: myClassName
          })
        }
        else if (callState === "missed") {
          if (redCircle) myClassName = 'redCircle';
          else myClassName = null;

          myIcon = L.icon({
            iconUrl: missedCallPng,
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            popupAnchor: [-15, 0],
            className: myClassName
          })
        }

        return L.marker(latlng, { icon: myIcon });

        //#region circleMarker
        let geojsonMarkerOptions = {
          radius: 8,
          weight: 1,
          opacity: 1,
          fillOpacity: 0.8
        };

        if (callState === "ringing") {
          geojsonMarkerOptions.fillColor = "green";
          geojsonMarkerOptions.color = "black";
        } else {
          geojsonMarkerOptions.fillColor = "yellow";
          geojsonMarkerOptions.color = "red";
        }

        return L.circleMarker(latlng, geojsonMarkerOptions);
        //#endregion
      },
    });
    markersClusterGroup.addLayer(LAGeojson);
    map.addLayer(markersClusterGroup);
  }

  function handleAddLabel() {
    let myIcon;
    const { current } = mapRef;
    const { leafletElement: map } = current;

    let LAGeojson = new L.GeoJSON(LAPoints, {
      onEachFeature: (feature = {}, layer) => {
        function handleClickPopup(e) {
          // console.log(e);
        };

        const myPopup = L.popup();
        const myHtmlTemplate = `
                        <div>
                            <h3> a point </h3>
                            <button onClick="handleClickPopup()">Answer</button>                        
                        </div>
                    `;
        // const myHtmlTemplate = <Popup name={name} website={website} />
        myPopup.setContent(myHtmlTemplate);
        layer.bindPopup(myPopup);
      },
      pointToLayer: function (feature = {}, latlng) {
        const { properties = {} } = feature;
        const { callState } = properties;

        myIcon = L.divIcon({
          html: `<div class="labelOnMap"> ${callState} <div/>`
        });

        return L.marker(latlng, { icon: myIcon });
      },
    });

    map.addLayer(LAGeojson)

  }

  const myMarkers = LAPoints.features.map((feat, index) => {
    if (feat.geometry.type === "Point") {

      let callState = feat.properties.callState;
      let redCircle = feat.properties.redCircle;
      let myClassName, myIcon;

      if (callState === "ringing") {
        if (redCircle) myClassName = 'redCircle';
        else myClassName = null;

        if (index <= 32) {
          myIcon = L.icon({
            iconUrl: ringingCallPng,
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            popupAnchor: [-15, 0],
            className: myClassName
          })
        }
        else {
          myIcon = L.divIcon({
            html: `<div class="labelOnMap"> ${callState} <div/>`
          });
        }
      }
      else if (callState === "missed") {
        if (redCircle) myClassName = 'redCircle';
        else myClassName = null;

        if (index <= 32) {
          myIcon = L.icon({
            iconUrl: missedCallPng,
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            popupAnchor: [-15, 0],
            className: myClassName
          })
        }
        else {
          myIcon = L.divIcon({
            html: `<div class="labelOnMap"> ${callState} <div/>`
          });
        }
      }

      return (
        <Marker
          key={index}
          position={[feat.geometry.coordinates[1], feat.geometry.coordinates[0]]}
          icon={myIcon} >
          <ReactLeafletPopup callState={callState} />
          <Tooltip> a tooltip </Tooltip>
        </Marker>
      )
    }
    else return null;
  });
  const HEREstyle = 'normal.day';;
  const HEREAPIKEY = 'ICUjdk2iphEDQYyb8ymusoTGcRJpL-6kjTbepQoLm90';
  return (

    <div id="container">
      {/* <button onClick={handleAddCircle}>Draw a circle</button>
            <button onClick={handleAddManyPoints}>Add many Points</button>
            <button onClick={handleAddLabel}>Add Labels</button> */}

      {/* coordinates for Los Angeles, but you can add your own */}
      <Map ref={mapRef} center={[33.91, -118.23]} zoom={11}>
        <TileLayer
          // // we'll be using OpenStreetMap (we'll change this in the later lessons)
          url={`https://2.base.maps.ls.hereapi.com/maptile/2.1/maptile/newest/${HEREstyle}/{z}/{x}/{y}/512/png8?apiKey=${HEREAPIKEY}&ppi=320`}
          //url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          // url={`http://t{s}.tiles.virtualearth.net/tiles/a{q}.jpeg?g=1398`}
          // don't forget to attribute them!
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        {/* <MarkerClusterGroup> */}
        {myMarkers}
        <Marker position={[33.91, -118.23]}>
          <Popup>
            <span>A pretty CSS3 popup. <br /> Easily customizable.</span>
          </Popup>
        </Marker>
        {/* </MarkerClusterGroup> */}
      </Map>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    ayEsm: dispatch({ type: 'INC' })
  }
}

export default LeafletMap;