import React from 'react';
import { Popup } from 'react-leaflet';

const ReactLeafletPopup = (props) => {
    const callState = props.callState;

    const handleClickPopup = (e) => {
        console.log(e)
    };

    return (<Popup>
        <div>
            <h3> a Popup {callState} </h3>
            <button onClick={handleClickPopup}>Answer</button>
        </div>
    </Popup>);
};

export default ReactLeafletPopup;